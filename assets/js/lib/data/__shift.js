const __basedir = process.cwd();
const date = require('date-and-time');
const helper = require(__basedir + '/class/helper.class');

class __breaks {
	constructor(data) {
		this._breaks = data;
		this.auth = {
			name: [['pengawas', 'operator']],
			group: 1,
			alias: 'PRODUCTION'
		};
	}

	list(redo = false) {
		if (redo || !this._breaks.list) {
			this._breaks.list = this._breaks.map(item => {
				return {
					...item,
					duration: helper.clockHMSDiff(item.start, item.finish)
				};
			});
		}

		return this._breaks.list;
	};

	totalDuration(redo = false) {
		if (redo || !this._breaks.totalDuration) {
			this._breaksTotalDuration = 0;
			for (const item of this._breaks) {
				this._breaksTotalDuration += (helper.clockHMSDiff(item.start, item.finish));
			}
		}

		return this._breaksTotalDuration;
	};
}

class __production {
	constructor(kanban) {
		this.kanban = kanban;
		// console.log('kanban',kanban);
		this.startTime = null;
		this.details = null;

		this._ngLogs = [];
		this._cb = {};
		this._total = {
			pcs: 0,
			ng: 0
		};
	}

	reset() {
		this.startTime = null;
		this._total = {
			pcs: 0,
			ng: 0
		};

		this._ngLogs = [];

		if (this._cb.onReset instanceof Function) {
			this._cb.onReset(this.summary);
		}
	};

	incPcs(count = 1) {
		if (!this.startTime) {
			return this.summary;
		}

		this._total.pcs += count;

		if (this._cb.onProximity instanceof Function) {
			this._cb.onProximity(this.details, this.summary);
		}

		return this.summary;
	};

	incNG(id, count = 1) {
		if (!this.startTime || this.summary.ok - count < 0) {
			return this.summary;
		}

		this._total.ng += count;
		this._ngLogs.push({
			id,
			timestamp: date.format(new Date(), 'YYYY-MM-DD HH:mm:dd')
		});

		if (this._cb.onProximity instanceof Function) {
			this._cb.onProximity(this.details, this.summary);
		}

		return this.summary;
	};

	get ok() {
		return this._total.pcs - this._total.ng;
	};

	get duration() {
		return (Date.now() - this.startTime);
	};

	get ct() {
		return this.ok > 0 ? this.duration / this.ok : this.details.plan.kanban.ct;
	};

	get kanbanDone() {
		return Math.floor(this.ok / this.kanban.qty);
	};

	get summary() {
		return {
			...this._total,
			ok: this.ok,
			ct: this.ct,
			kanbanDone: this.kanbanDone,
			duration: this.duration
		};
	};
}

class __shift {
	init(shift, interval = 500) {
		this._data = shift;
		this._interval = interval;
		// this.breaks = new __breaks(shift.breaks);
		// this.production = new __production(shift.plan.kanban);

		// this._cb = {};
	};

	start(startTime) {
		this.production.reset();
		this.production.startTime = startTime;
		this.production.details = this.details;

		if (this._cb.onStart instanceof Function) {
			this._cb.onStart(this.details, this.production.summary);
		}

		if (this._cb.onInterval instanceof Function) {
			this._onInterval = setInterval(() => {
				this._cb.onStart(this.details, this.production.summary);
			}, this._interval);
		}
	};

	setCallback(event, fn) {
		if (fn instanceof Function) {
			// this._cb[event] = fn;
			// this.production._cb[event] = fn;
			return this;
		}

		return false;
	};

	get details() {
		return {
			id: this._data.id,
			name: this._data.name,
			...this._data
		};
	};
}

module.exports = new __shift();
