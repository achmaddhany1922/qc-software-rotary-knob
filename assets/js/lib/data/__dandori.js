class __dandori {
	init(_list) {
		this.dandori = _list;
		this._scw = {
			status: false,
			auth: [
				{
					name: ['utility'],
					group: 0,
					alias: undefined
				},
				{
					name: [['pengawas', 'operator']],
					group: 1,
					alias: 'PRODUCTION'
				}
			],
			duration: 0
		};
	};

	get scw() {
		return this._scw.status ? this._scw : null;
	};

	get list() {
		return this.dandori;
	};

	details(id) {
		for (const item of this.dandori) {
			if (item.id === id) {
				return item;
			}
		}
	};

	startSCW() {
		this._scw.status = true;

		this.problem = {
			...this.problem,
			scw: this._scw
		};

		return this.problem;
	};

	start(id) {
		this.problem = {
			...this.details(id)
		};

		return this.problem;
	};

	current(key) {
		const _current = this.problem ? this.problem : {};

		return key && _current ? _current[key] : _current;
	};

	async finish(duration) {
		const data = {
			...this.problem,
			duration
		};

		delete this.problem;

		return data;
	};
}

module.exports = new __dandori();
