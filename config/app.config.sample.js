module.exports = {
	arduino1: {
		port: '/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0',
		baud: 38400,
	},

	motor: {
		port: '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_55736313737351209062-if00',
		baud: 38400,
	},

	window: {
		width : 1280,
		height : 800,
		fullscreen : false,
	},

	limits: {
		counter: 0,
		round: 250,
		total: 10,
		timeout: 300000,
	},

	target: {
		ticksPerSecond: 3,
		tolerance: 0.025,
	},

	autoreload: true,
};
