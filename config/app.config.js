module.exports = {
	arduino: [ 
	{
		id: 1,
		port: 'COM7',
		baud: 38400,
	},
	{
		id: 2,
		port: 'COM8',
		baud: 38400,
	},
	{
		id: 3,
		port: '',
		baud: 38400,
	},
	{
		id: 4,
		port: '',
		baud: 38400,
	}],

	motor: {
		port: '',
		baud: 38400,
	},

	window: {
		width : 1280,
		height : 800,
		fullscreen : false,
	},

	limits: {
		counter: 0,
		round: 250,
		total: 10,
		timeout: 300000,
	},

	target: {
		ticksPerSecond: 3,
		tolerance: 0.025,
	},
	debug: true,
	devTools: true,
	autoreload: true,
};
